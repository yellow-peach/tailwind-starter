<?php
$block = get_field('latest_news');
$styling = get_field('block_styling');
$spacing = get_block_spacing();
$colour_class = get_block_colour_class();
$button_class = get_block_button_colour_class();
?>

<section
		class="section-wrapper latest-news <?php if ($spacing) echo $spacing; ?> <?php if ($styling['background_colour']) echo 'block-bg-' . $styling['background_colour']; ?>">

	<div class="container">
		<?php if ($block['heading']) echo '<h2 class="' . $colour_class . '">' . $block['heading'] . '</h2>'; ?>

		<div data-aos="fade-in">

			<?php
			$news = new WP_Query(array(
					'post_type' => 'post',
					'posts_per_page' => 3,
			));
			if ($news->have_posts()):
				while ($news->have_posts()): $news->the_post();
					echo '<div>';
					include get_template_directory() . '/template-parts/news-item.php';
					echo '</div>';
				endwhile;
			endif;
			?>

		</div>

		<div class=" text-center mt-5">
			<a href="<?php echo get_permalink(get_option('page_for_posts')); ?>" class="<?php echo $button_class; ?>">View
				more</a>
		</div>

	</div>

</section>
