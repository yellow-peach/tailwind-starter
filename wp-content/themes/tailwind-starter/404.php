<?php get_header(); ?>


<div class="page-banner">
	<div class="container">
			<div class="text-center">
				<h1><?php echo _e('Page cannot be found', 'wp-starter'); ?></h1>
				<h3 class="mt-2"><?php _e('Oops, it looks like nothing was found at this location.'); ?></h3>
				<div class="btn-set mb-10 mt-10">
					<a href="<?php echo home_url(); ?>" class="y-btn"
					   title="Return home"><?php _e('Return home', 'wp-starter'); ?></a>
					<a href="<?php echo site_url('/contact-us/'); ?>" class="y-btn"
					   title="Get in touch"><?php _e('Contact us', 'wp-starter'); ?></a>
				</div>

			</div>
	</div>
</div>


<?php get_footer(); ?>
