<footer>
	<div class="container">
		<p class="">&copy; <?php echo date("Y"); ?> <?php if(get_field('company_name', 'options')) echo get_field('company_name', 'options'); ?></p>
		<p class=""><a href="https://yellowpeach.co.uk" target="_blank" rel="nofollow" class="">Website by Yellow Peach</a></p>
	</div>
</footer>

<?php wp_footer() ?>
</body>
</html>

