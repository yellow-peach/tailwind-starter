<?php
$block = get_field('full-width_content');
$styling = get_field('block_styling');
$spacing = get_block_spacing($styling, true);
$colour_class = get_block_colour_class();
$button_class = get_block_button_colour_class();
$btn_set = isset($block['buttons']) ? get_button_set($block['buttons'], ' mt-5', $button_class) : '';
$sub_colour = 'text-grey-light';
if ($styling['background_colour'] == 'blue' || $styling['background_colour'] == 'black') {
	$sub_colour = 'text-yellow';
}
?>

<section
		class="section-wrapper full-width-content <?php if ($spacing) echo $spacing; ?> <?php if ($styling['background_colour']) echo 'bg-' . $styling['background_colour']; ?>">

	<div class="container">


		<div class="<?php if ($block['center_block']) echo ' text-center'; ?>" data-aos="fade-in"
			 data-aos-delay="300">

			<?php if ($block['sub_heading']) echo '<h6 class="mb-h6 ' . $sub_colour . '">' . $block['sub_heading'] . '</h6>'; ?>
			<?php if ($block['heading']) echo '<h2 class="text-black mb-5 ' . $colour_class . '">' . $block['heading'] . '</h2>'; ?>
			<?php if ($block['content']) echo '<div class=" ' . $colour_class . '">' . $block['content'] . '</div>'; ?>

			<?php echo $btn_set; ?>

		</div>

	</div>

 
</section>

