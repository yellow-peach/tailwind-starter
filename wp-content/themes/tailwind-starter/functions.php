<?php
/**
 * Kickoff theme setup and build
 */

require get_stylesheet_directory() . '/includes/theme-functions.php';
require get_stylesheet_directory() . '/includes/admin-functions.php';
require get_stylesheet_directory() . '/includes/misc-functions.php';
require get_stylesheet_directory() . '/includes/gutenberg-functions.php';
