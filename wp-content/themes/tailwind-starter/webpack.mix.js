let mix = require('laravel-mix');
let path = require('path');

mix.setPublicPath(path.resolve('./'));

mix.js('resources/js/app.js', 'js');

mix.postCss("resources/css/app.css", "css");

mix.postCss("resources/css/gutenberg-style.css", "./", [
	(require('postcss-editor-styles'))({
		scopeTo: '.editor-styles-wrapper',
	})
]);

mix.postCss("resources/css/editor-style.css", "./", [
	(require('postcss-editor-styles'))({
		scopeTo: '.editor-styles-wrapper .acf-block-preview',
		tagSuffix: ''
	})
]);

mix.options({
	postCss: [
		require('postcss-import'),
		require('tailwindcss/nesting'),
		require('tailwindcss'),
		require('autoprefixer'),
	]
});

mix.browserSync({
	proxy: 'http://tailwind-starter.localhost',
	files: [
		'**/*.php',
		'./resources/',
		'**/*.css'
	],
});

mix.version();


mix.combine([], './css/vendor.css');

mix.combine([], './js/vendor.js');
