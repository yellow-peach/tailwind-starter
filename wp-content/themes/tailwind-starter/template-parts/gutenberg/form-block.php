<?php
$block = get_field('form_block');
$styling = get_field('block_styling');
$spacing = get_block_spacing($styling, true);
$colour_class = get_block_colour_class();
$button_class = get_block_button_colour_class();
?>


<section
		class="section-wrapper form-block <?php if ($spacing) echo $spacing; ?> <?php if ($styling['background_colour']) echo 'bg-' . $styling['background_colour']; ?>">

	<div class="container">
		<div class="md:grid md:grid-cols-2">
			<?php if ($block['content']) echo '<div class="md:pr-20 mb-5 md:mb-0 ' . $colour_class . '" data-aos="fade-in">' . $block['content'] . '</div>'; ?>

			<?php
			if ($block['form_id']) {
				echo '<div class="form-wrapper ' . str_replace('text', 'form', $colour_class) . '">';
				gravity_form($block['form_id'], false, false, false, null, true);
				echo '</div>';
			}
			?>


		</div>
	</div>


</section>
