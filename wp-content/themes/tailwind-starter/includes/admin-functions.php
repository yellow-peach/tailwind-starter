<?php

if (function_exists('acf_add_options_page')) {

	acf_add_options_page(array(
		'page_title' => 'General Settings',
		'menu_title' => 'General Settings',
		'menu_slug' => 'general-settings',
		'capability' => 'edit_posts',
		'parent_slug' => '',
		'position' => false,
		'icon_url' => false,
		'redirect' => false
	));

}


// Allow editors to access Gravity Forms
add_action( 'admin_init', function () {
	$role = get_role( 'editor' );
	$role->add_cap( 'gravityforms_create_form' );
	$role->add_cap( 'gravityforms_delete_forms' );
	$role->add_cap( 'gravityforms_edit_forms' );
	$role->add_cap( 'gravityforms_preview_forms' );
	$role->add_cap( 'gravityforms_view_entries' );
	$role->add_cap( 'gravityforms_edit_entries' );
	$role->add_cap( 'gravityforms_delete_entries' );
	$role->add_cap( 'gravityforms_view_entry_notes' );
	$role->add_cap( 'gravityforms_edit_entry_notes' );
	$role->add_cap( 'gravityforms_export_entries' );
} );


// Alow SVG file type in media library
add_filter('upload_mimes', function ($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
});
