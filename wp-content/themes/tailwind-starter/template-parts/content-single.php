<article id="post-<?php the_ID(); ?>" <?php post_class('blog-single-content '); ?>>
	<div class="blog-content">
		<?php the_content(); ?>
		<span class="blog-publish-date">Published: <?php the_date('jS F o'); ?></span>
	</div>
</article>

