<?php
$hero = get_field('hero_banner');

?>

<section class="hero-banner">
	<div class="
    <?php if (isset($hero['banners']) && count($hero['banners']) > 1):
		echo 'swiper-container hero-carousel';
	else:
		echo 'hero-single-carousel';
	endif; ?>">

		<div class="swiper-wrapper">
			<?php foreach ($hero['banners'] as $banner) : ?>
				<?php $btn_set = isset($banner['buttons']) ? get_button_set($banner['buttons'], '', '', '') : ''; ?>
				<div class="swiper-slide">
					<?php if ($banner['image']): ?>
						<div class="image-wrapper">
							<img <?php responsive_img($banner['image'], 'extra-large'); ?>
									alt="<?php $banner['image']['alt']; ?>"/>
						</div>
					<?php endif; ?>
					<div class="container">
						<div class="text-center">
							<div class="hero-content">
								<?php if ($banner['heading']) echo '<h1 data-aos="fade-in" data-aos-delay="150">' . $banner['heading'] . '</h1>'; ?>
								<?php if ($banner['content']) echo '<div data-aos="fade-in" data-aos-delay="150">' . $banner['content'] . '</div>'; ?>
								<?php echo $btn_set; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<div class="banner-pagination"></div>

	</div>
</section>

