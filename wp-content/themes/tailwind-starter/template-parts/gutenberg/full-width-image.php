<?php
$block = get_field('full-width_image');
$styling = get_field('block_styling');

if ($block['image']): ?>

	<section class="full-width-image" data-aos="fade-in">
		<div class="image-wrapper">
			<img <?php responsive_img($block['image'], 'large'); ?> alt="<?php $block['image']['alt']; ?>"
																	class="object-cover w-screen h-h600"/>
		</div>
	</section>

<?php endif; ?>

