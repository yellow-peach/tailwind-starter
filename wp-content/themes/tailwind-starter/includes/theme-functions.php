<?php

/**
 * Enqueue scripts.
 */


add_action('wp_enqueue_scripts', function () {
	$theme = wp_get_theme();

	wp_enqueue_script('jquery');

	wp_enqueue_style('app', get_mix_compiled_asset_url('css/app.css'), array(), $theme->get('Version'));
	wp_enqueue_script('app', get_mix_compiled_asset_url('js/app.js'), array('jquery'), $theme->get('Version'));
});

add_action('admin_enqueue_scripts', function ($hook) {

	if ('post.php' == $hook && isset($_GET['action']) && $_GET['action'] == 'edit') {
		$theme = wp_get_theme();
		wp_enqueue_style('yp-editor-style', get_mix_compiled_asset_url('editor-style.css'), array(), $theme->get('Version'));
		wp_enqueue_style('yp-gutenberg-style', get_mix_compiled_asset_url('gutenberg-style.css'), array(), $theme->get('Version'));
		wp_enqueue_style('font', 'https://use.typekit.net/gbp3yqq.css', array());
	}

});


// Inline styles for Gutenberg editor
add_action('admin_head', function () {
	echo '<style>
		.wp-block.wp-block-post-title {
		  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif;
		  margin-bottom: 0.8em !important;
		  font-size: 2.5em;
		  font-weight: 800;
		}

		@media (min-width: 768px) {
		.interface-complementary-area {
			width: 420px;
		}
	}</style>';

});


//Remove JQuery migrate
add_action('wp_default_scripts', function ($scripts) {
	if (!is_admin() && isset($scripts->registered['jquery'])) {
		$script = $scripts->registered['jquery'];

		if ($script->deps) { // Check whether the script has any dependencies
			$script->deps = array_diff($script->deps, array(
				'jquery-migrate'
			));
		}
	}
});


/**
 * Get mix compiled asset.
 *
 * @param string $path The path to the asset.
 *
 * @return string
 */
function get_mix_compiled_asset_url($path)
{
	$path = '/' . $path;
	$stylesheet_dir_uri = get_stylesheet_directory_uri();
	$stylesheet_dir_path = get_stylesheet_directory();

	if (!file_exists($stylesheet_dir_path . '/mix-manifest.json')) {
		return $stylesheet_dir_uri . $path;
	}

	$mix_file_path = file_get_contents($stylesheet_dir_path . '/mix-manifest.json');
	$manifest = json_decode($mix_file_path, true);
	$asset_path = !empty($manifest[$path]) ? $manifest[$path] : $path;

	return $stylesheet_dir_uri . $asset_path;
}

/**
 * Get data from the theme-configuration.json file.
 *
 * @param mixed $key The key to retrieve.
 *
 * @return mixed|null
 */
function get_theme_configuration($key = null)
{
	$config = json_decode(file_get_contents(get_stylesheet_directory() . '/theme-configuration.json'), true);

	if ($key === null) {
		return filter_var_array($config, FILTER_SANITIZE_STRING);
	}

	$option = filter_var($config[$key], FILTER_SANITIZE_STRING);

	return $option ?? null;
}

/**
 * Theme setup.
 */
function theme_setup()
{

	// Let WordPress manage the document title.
	add_theme_support('title-tag');

	register_nav_menu('main-navigation', __('Main Navigation'));
	register_nav_menu('footer-navigation-1', __('Footer Navigation 1'));
	register_nav_menu('footer-navigation-2', __('Footer Navigation 2'));
	register_nav_menu('foot-navigation', __('Foot Navigation'));

	// Adding Thumbnail basic support.
	add_theme_support('post-thumbnails');

	// Block editor.
	add_theme_support('align-wide');

	add_theme_support('wp-block-styles');

	add_theme_support('editor-styles');
	remove_editor_styles();

}

add_action('after_setup_theme', 'theme_setup');


/**********************************************
 ** Navigations/Menus
 **********************************************/

/*
** Register custom image sizes
*/
add_image_size('extra-large', 1800, 800, false);
add_image_size('hero', 1400, 500, false);
add_image_size('page-banner', 1800, 0, false);
add_image_size('xl-hero', 2000, 1200, false);


/**********************************************
 ** Content
 **********************************************/

/*
** Custom excerpt length
*/

add_filter('excerpt_length', function () {

	return 35;

}, 999);

/*
** Custom excerpt ending
*/
add_filter('excerpt_more', function () {

	return '...';

});


function responsive_img($image_arr, $image_size)
{

	if (is_array($image_arr)) {

		$image_src = wp_get_attachment_image_url($image_arr['id'], $image_size);

		$image_srcset = wp_get_attachment_image_srcset($image_arr['id'], $image_size);

		$image_sizes = wp_get_attachment_image_sizes($image_arr['id'], $image_size);

		return 'src="' . $image_src . '" srcset="' . $image_srcset . '" sizes="' . $image_sizes . '"';

	} else {
		
		return 'src="' . $image_arr[$image_size] . '"';

	}

}



