window.$ = jQuery;

/*
 * Swiper uncomment where required
 */
// import Swiper, {Navigation} from 'swiper';
// import 'swiper/css';
// import 'swiper/css/navigation';
// import 'swiper/css/pagination';
// Swiper.use([Navigation]);

/*
 * AOS uncomment where required
 */
import AOS from 'aos';

$(document).ready(function () {
	AOS.init();
});
