<?php


// Disable all the core Gutenberg blocks
function filter_allowed_block_types_when_post_provided($allowed_block_types, $editor_context)
{
	if (!empty($editor_context->post)) {
		return array(
			'acf/hero-banner',
			'acf/full-width-content',
			'acf/left-right-content',
			'acf/latest-news',
			'acf/full-width-image',
			'acf/columned-content',
			'acf/call-to-action',
			'acf/page-banner',
			'acf/form-block',
		);
	}
	return $allowed_block_types;
}

add_filter('allowed_block_types_all', 'filter_allowed_block_types_when_post_provided', 10, 2);


// Register category for our blocks
add_filter('block_categories_all', function ($cats, $post) {

	return array_merge(
		array(
			array(
				'slug' => 'yellow-peach',
				'title' => get_bloginfo('name') . ' Blocks',
				'icon' => 'admin-appearance',
			),
		), $cats
	);

}, 1, 2);


add_action('acf/init', function () {


	if (function_exists('acf_register_block_type')) {

		include_once(ABSPATH . 'wp-admin/includes/plugin.php');

		acf_register_block_type(array(
			'name' => 'hero-banner',
			'title' => __('Hero Carousel'),
			'description' => __('A carousel of banners.'),
			'post_types' => array('page', 'post'),
			'render_callback' => 'yp_render_block',
			'category' => 'yellow-peach',
			'mode' => 'preview',
			'icon' => 'format-aside',
			'keywords' => array('hero', 'home', 'banner', 'carousel'),
			'supports' => array(
				'mode' => true,
			),
		));


		acf_register_block_type(array(
			'name' => 'left-right-content',
			'title' => __('Left/Right content'),
			'description' => __('A left/right block with image & content.'),
			'post_types' => array('page', 'post'),
			'render_callback' => 'yp_render_block',
			'category' => 'yellow-peach',
			'mode' => 'preview',
			'icon' => 'align-left',
			'keywords' => array('left', 'right', 'column', 'content', 'image'),
			'supports' => array(
				'mode' => true,
			),
		));


		acf_register_block_type(array(
			'name' => 'columned-content',
			'title' => __('Columned content'),
			'description' => __('A flexible multi-column content block'),
			'post_types' => array('page', 'post'),
			'render_callback' => 'yp_render_block',
			'category' => 'yellow-peach',
			'mode' => 'preview',
			'icon' => 'editor-table',
			'keywords' => array('content', 'column', 'text'),
			'supports' => array(
				'mode' => true,
			),
		));


		acf_register_block_type(array(
			'name' => 'latest-news',
			'title' => __('Latest news'),
			'description' => __('Displays the 3 latest news posts.'),
			'post_types' => array('page', 'post'),
			'render_callback' => 'yp_render_block',
			'category' => 'yellow-peach',
			'mode' => 'preview',
			'icon' => 'sticky',
			'keywords' => array('post', 'latest', 'news', 'blog'),
			'supports' => array(
				'mode' => true,
			),
		));


		acf_register_block_type(array(
			'name' => 'call-to-action',
			'title' => __('Call to action'),
			'description' => __('A call to action banner.'),
			'post_types' => array('page', 'post'),
			'render_callback' => 'yp_render_block',
			'category' => 'yellow-peach',
			'mode' => 'preview',
			'icon' => 'smartphone',
			'keywords' => array('call', 'banner', 'cta', 'contact'),
			'supports' => array(
				'mode' => true,
			),
		));


		acf_register_block_type(array(
			'name' => 'full-width-image',
			'title' => __('Full width image'),
			'description' => __('A full width banner image'),
			'post_types' => array('page', 'post'),
			'render_callback' => 'yp_render_block',
			'category' => 'yellow-peach',
			'mode' => 'preview',
			'icon' => 'format-image',
			'keywords' => array('image', 'banner'),
			'supports' => array(
				'mode' => true,
			),
		));


		acf_register_block_type(array(
			'name' => 'page-banner',
			'title' => __('Page banner'),
			'description' => __('A banner for displaying the page heading, breadcrumb and image'),
			'post_types' => array('page', 'post'),
			'render_callback' => 'yp_render_block',
			'category' => 'yellow-peach',
			'mode' => 'preview',
			'icon' => 'editor-bold',
			'keywords' => array('title', 'heading', 'banner'),
			'supports' => array(
				'mode' => true,
			),
		));


		acf_register_block_type(array(
			'name' => 'full-width-content',
			'title' => __('Full-width content'),
			'description' => __('A basic full-width content block'),
			'post_types' => array('page', 'post'),
			'render_callback' => 'yp_render_block',
			'category' => 'yellow-peach',
			'mode' => 'preview',
			'icon' => 'editor-justify',
			'keywords' => array('content', 'block', 'full', 'text'),
			'supports' => array(
				'mode' => true,
			),
		));


		acf_register_block_type(array(
			'name' => 'form-block',
			'title' => __('Form block'),
			'description' => __('A block to display an enquiry form.'),
			'post_types' => array('page', 'post'),
			'render_callback' => 'yp_render_block',
			'category' => 'yellow-peach',
			'mode' => 'preview',
			'icon' => 'list-view',
			'keywords' => array('form', 'enquiry', 'contact'),
			'supports' => array(
				'mode' => true,
			),
		));


	}

});


function yp_render_block($block)
{

	static $prev_block_colour;
	$block_colour = '';

	if (isset($block['data']['block_styling_background_colour'])) {
		$block_colour = $block['data']['block_styling_background_colour'];
	}

	$slug = str_replace('acf/', '', $block['name']);


	if (file_exists(get_theme_file_path("/template-parts/gutenberg/{$slug}.php"))) {

		include(get_theme_file_path("/template-parts/gutenberg/{$slug}.php"));

	}

	$prev_block_colour = $block_colour;

}


add_filter('render_block', function ($block_content, $block) {

	if (stripos(trim($block['blockName']), "core") === 0 || $block['blockName'] == 'tadv/classic-paragraph') {
		if ($block['blockName'] == 'core/cover' || $block['blockName'] == 'core/column') {
			return $block_content;
		}


		$block_content =
			'<div class="core-block-section" data-aos="fade-in" >
			   <div class="container" >' . $block_content . '</div >
		</div > ';


	}

	return $block_content;

}, 10, 2);


/*
**  Fix, due to bug currently with Gutenberg that adds extra <p> and <br> tags throughout blocks when using the_excerpt or the_content
** (See: https://github.com/WordPress/gutenberg/issues/12530)
*/
remove_filter('the_content', 'wpautop');
add_filter('the_content', function ($content) {

	if (has_blocks()) {

		return $content;

	}

	return wpautop($content);

});


// Remove default editor styles
add_filter('block_editor_settings_all', function ( $editor_settings, $editor_context ) {
	if ( ! empty( $editor_context->post ) ) {
		unset($editor_settings['defaultEditorStyles'][0]);
	}

	return $editor_settings;
}, 10, 2);


// Disable gutenberg on posts
function disabled_gutenberg_posts($can_edit, $post_type)
{

	if (empty($post_type)) return $can_edit;

	// if ('post' === $post_type ) return false;

	return $can_edit;

}

add_filter('use_block_editor_for_post_type', 'disabled_gutenberg_posts', 10, 2);


function get_block_spacing($fields, $output = false)
{

	if (!$fields) {
		return;
	}

	$classes = array(
		'top' => 'spacing-top-' . $fields['spacing_above'],
		'bottom' => 'spacing-bottom-' . $fields['spacing_below']
	);

	if ($output) {

		return $classes['top'] . ' ' . $classes['bottom'];

	}

	return $classes;

}


function get_block_colour_class($bg = false)
{

	$colour_class = '';

	if (!$bg) {
		$bg = get_field('block_styling')['background_colour'];
	}


	if ($bg && ($bg == 'teal' || $bg == 'dark-teal')) {

		$colour_class = 'text-white';

	}


	return $colour_class;

}

function get_block_button_colour_class($bg = false, $reversed = false)
{

	$button_class = 'btn y-btn';

	// Reversed button class
	if ($reversed) {

		$button_class = 'y-btn btn-teal-reversed';

	}

	if (!$bg) {
		$bg = get_field('block_styling')['background_colour'];
	}

	// Set different button class when there is a clashing background colour
	if ($bg && ($bg == 'teal' || $bg == 'dark-teal')) {

		$button_class = 'y-btn btn-white';

		if ($reversed) {

			$button_class = 'y-btn btn-white-reversed';

		}

	}

	return $button_class;

}


function get_button_set($repeater_field, $wrapper_class = false, $btn_class = false, $wrapper_attrs = false) {

	if(!is_array($repeater_field)) {
		return;
	}

	$html = '';

	if ($repeater_field) {
		$html .= '<div class="btn-set ' . $wrapper_class . '" data-aos="fade-in" ' . $wrapper_attrs . ' >';
		foreach ($repeater_field as $btn) {
			if($btn['button']) {
				$html .= '<a href="' . $btn['button']['url'] . '" title="' . $btn['button']['title'] . '" class="y-btn ' . $btn_class . '" target="' . $btn['button']['target'] . '" >' . $btn['button']['title'] . '</a>';
			}
		}
		$html .= '</div>';
	}

	return $html;

}
