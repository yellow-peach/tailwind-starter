<?php

// Disable admin bar for all users
show_admin_bar(false);


// Remove wp-embed.js
add_action('wp_footer', function () {
	wp_deregister_script('wp-embed');
});

/*
** Removing window._wpemojiSettings from header.
*/
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');


// Let WordPress manage the document title.
add_theme_support('title-tag');


/*
** API Key for ACF Google Maps field
*/
add_action('acf/init', function () {

	if (get_field('google_maps_api_key', 'options')) {
		acf_update_setting('google_api_key', get_field('google_maps_api_key', 'options'));
	}

});


// Move Yoast to bottom
function yoasttobottom()
{
	return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');

