<?php
if (is_shop()) {
	$title = get_the_title(get_option('woocommerce_shop_page_id'));
} else if (is_product_category()) {
	$title = get_queried_object()->name;
} else {
	$title = get_the_title();
}
?>

<header class="shop-banner">
	<div class="container">

		<?php woocommerce_breadcrumb(); ?>

	</div>
</header>

