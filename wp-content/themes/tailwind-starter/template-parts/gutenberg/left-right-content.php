<?php
$block = get_field('left_right_content');
$styling = get_field('block_styling');
$spacing = get_block_spacing($styling, true);
$colour_class = get_block_colour_class();
$button_class = get_block_button_colour_class();
$btn_set = isset($block['buttons']) ? get_button_set($block['buttons'], ' mt-5', $button_class) : '';

?>


<section
		class="section-wrapper left-right-content <?php if ($spacing) echo $spacing; ?> <?php if ($styling['background_colour']) echo 'bg-' . $styling['background_colour']; ?> ">

	<div class="container mx-auto">

		<div class="grid grid-cols-1 items-center md:grid-cols-2 ">

			<?php if ($block['left_image']): ?>

				<div class="<?php if ($block['swap_positioning']) echo 'order-2'; ?>">
					<img <?php responsive_img($block['image'], 'large'); ?>
							alt="<?php $block['image']['alt']; ?>" class="object-cover object-center w-full"/>
				</div>

			<?php endif; ?>

			<?php if ($block['right_content']): ?>
				<div class="p-5 <?php if ($block['swap_positioning']) echo 'md:ml-5'; else echo 'md:mr-5'; ?>">
					<?php if ($block['right_content']['content']) echo '<div class="' . $colour_class . '">' . $block['right_content']['content'] . '</div>'; ?>
					<?php if ($block['right_content']['button']) echo '<div class="mt-5"><a href="' . $block['right_content']['button']['url'] . '" class=" y-btn ' . $button_class . '" target="' . $block['right_content']['button']['target'] . '">' . $block['right_content']['button']['title'] . '</a></div>'; ?>
				</div>
			<?php endif; ?>

		</div>

	</div>

</section>


