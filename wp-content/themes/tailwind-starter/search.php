<?php get_header(); ?>

<div class="page-banner">
	<div class="container">
			<div class="text-center">
				<?php
				if (function_exists('yoast_breadcrumb')) {
					yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
				}
				?>
				<h1 class="page-title mb-5"><?php printf(__('Search Results for: %s', 'twentysixteen'), '<span>' . esc_html(get_search_query()) . '</span>'); ?></h1>

			</div>
	</div>
</div>


<div class="container pb-5">

	<?php if (have_posts()) : ?>

		<?php
		// Start the loop.
		while (have_posts()) : the_post();

			/**
			 * Run the loop for the search to output the results.
			 * If you want to overload this in a child theme then include a file
			 * called content-search.php and that will be used instead.
			 */
			get_template_part('template-parts/content', 'search');

			// End the loop.
		endwhile;

		// Previous/next page navigation.
		the_posts_pagination(array(
				'prev_text' => __('Previous page', 'twentysixteen'),
				'next_text' => __('Next page', 'twentysixteen'),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __('Page', 'wp-starter') . ' </span>',
		));

	// If no content, include the "No posts found" template.
	else :
		get_template_part('template-parts/content', 'none');

	endif;
	?>
</div>

<?php get_footer(); ?>
