<?php get_header(); ?>

<?php echo get_template_part('template-parts/page-banner'); ?>

<section class="news-single">
    <div class="container">

            <?php
            if (have_posts()) :

                while (have_posts()) : the_post(); ?>

					<?php
					$cat_link = '';
					$categories = get_the_category();
					if($categories) {
						$category  = $categories[0];
						$cat_link = '<a href="'.get_term_link($category->term_id).'">'.$category->name.'</a>';
                    }

					?>

                    <div class="news-meta">
                        <p class="news-date"><time><?php echo get_the_date('jS F Y', get_the_ID()); ?></time></p>
                        <?php if($cat_link) echo '<p class="news-cat">' . $cat_link . '</p>'; ?>
                    </div>

                    <?php the_content(); ?>

                    <div class="news-next-prev-links">
						<div class="prev"><?php previous_post_link(  $format = '<i class="icofont-arrow-left"></i> %link', $link = '%title'); ?></div>
						<div class="next"><?php next_post_link(  $format = '%link <i class="icofont-arrow-right"></i>', $link = '%title'); ?></div>
                    </div>



                <?php if (get_the_post_thumbnail(get_the_ID())): ?>

                        <?php echo get_the_post_thumbnail(get_the_ID(), 'large', array('class' => 'object-cover mb-5')); ?>


                <?php endif; ?>

                <?php endwhile;

            endif;
            ?>


    </div>
</section>

<?php get_footer(); ?>
