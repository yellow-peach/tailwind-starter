<?php get_header(); ?>

<?php if(!get_field('hide_page_title')) {
	echo get_template_part('template-parts/page-banner');
} ?>

<?php
if (have_posts()) :
	while (have_posts()) : the_post();

		the_content();

	endwhile;

endif;
?>

<?php get_footer(); ?>
