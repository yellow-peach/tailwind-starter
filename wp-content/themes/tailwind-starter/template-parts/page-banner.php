<div class="page-banner" data-aos="fade-in">
	<div class="container mx-auto">
		<div class="py-4">
			<?php
			if (function_exists('yoast_breadcrumb')) {
				yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
			}
			?>
			<?php if (is_home()): ?>
				<?php $blog_page_id = get_option('page_for_posts'); ?>
				<h1 class="page-heading"><?php echo get_the_title($blog_page_id); ?></h1>
			<?php elseif (is_category()): ?>
				<?php $category = get_queried_object(); ?>
				<h1 class="page-heading"><?php echo $category->name; ?></h1>
			<?php else: ?>
				<h1 class="page-heading"><?php the_title(); ?></h1>
			<?php endif; ?>
		</div>
	</div>
</div>


