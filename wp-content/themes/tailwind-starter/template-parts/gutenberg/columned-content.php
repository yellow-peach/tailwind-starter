<?php
$block = get_field('columned_content');
$styling = get_field('block_styling');
$spacing = get_block_spacing($styling, true);
$colour_class = get_block_colour_class();
$button_class = get_block_button_colour_class();
?>

<section
		class="section-wrapper multi-column-content <?php if ($spacing) echo $spacing; ?> <?php if ($styling['background_colour']) echo 'bg-' . $styling['background_colour']; ?>">
	<div class="container mx-auto">
		<?php if ($block['columns']) : ?>

			<?php
			if (count($block['columns']) > 4) {
				$cols = 4;
			} else {
				$cols = count($block['columns']);
			} ?>


			<div class="grid grid-cols-<?php echo $cols; ?> gap-10">

				<?php foreach ($block['columns'] as $column) : ?>

					<?php $btn_set = isset($column['buttons']) ? get_button_set($column['buttons'], ' mt-5', $button_class) : ''; ?>

					<div class="<?php if ($column['center_column']) echo 'text-center'; ?>">
						<?php if ($column['image']) echo '<div class=""><img ' . responsive_img($block['image'], 'large') . ' alt="' . $block['image']['alt'] . '" class="object-cover h-64 w-full"/></div>'; ?>

						<?php if ($column['content']) echo '<div class="' . $colour_class . '">' . $column['content'] . '</div>'; ?>

						<?php echo $btn_set; ?>

					</div>

				<?php endforeach; ?>

			</div>

		<?php endif; ?>

	</div>

</section>
