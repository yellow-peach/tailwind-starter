<?php get_header(); ?>
<?php //echo get_template_part('template-parts/page-banner'); ?>
<div class="container mt-20">
	<div class="flex">
		<div class="py-10">
			<h1>This is a heading 1</h1>
			<h2>This is a heading 2</h2>
			<h3>This is a heading 3</h3>
			<h4>This is a heading 4</h4>
			<h5>This is a heading 5</h5>
			<h6>This is a heading 6</h6>
			<div class="section-sub-heading">Section sub heading</div>
			<div class="section-heading">Section heading</div>
			<div class="hero-heading">Hero heading</div>

		</div>

	</div>

	<div class="border-t-2 border-b-2 py-10">
		<h3>Paragraphs</h3>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse fringilla condimentum sapien a
			tristique. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed
			faucibus orci pretium sollicitudin placerat. Aenean faucibus placerat ligula vitae imperdiet. Sed mattis
			tortor a ullamcorper fringilla. Vestibulum dapibus vestibulum felis at ornare. Suspendisse potenti. Donec
			rhoncus dui eu tortor ornare, ut lacinia nisi pretium. Fusce lacinia malesuada facilisis. Vestibulum ante
			ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec imperdiet sem justo, quis
			venenatis sapien egestas sit amet. Cras quis leo eget elit venenatis imperdiet sit amet at orci. Morbi a ex
			at tortor tristique rutrum non eu ipsum. Suspendisse sapien augue, consequat et tortor sed, porttitor
			facilisis risus.
		</p>
		<p>
			Phasellus pulvinar erat id dapibus hendrerit. Vivamus aliquam massa laoreet, gravida leo sed, posuere risus.
			Pellentesque ac felis at urna interdum facilisis nec ac nisi. Sed accumsan dui nec neque tristique
			elementum. Curabitur eu venenatis augue. Nam efficitur mauris quis placerat facilisis. Duis at est sagittis,
			rutrum massa at, bibendum sapien. Suspendisse potenti. Integer sagittis fermentum ipsum quis consequat.
			Quisque dignissim ullamcorper urna, non bibendum leo. Nam nulla enim, gravida eget nisi in, maximus
			venenatis risus.

			Sed porttitor porttitor facilisis. Aenean ante turpis, lacinia et mauris vel, scelerisque ornare tellus.
			Pellentesque eget augue id lorem viverra malesuada at eu nulla. Nulla eleifend convallis viverra. Vestibulum
			ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nulla ut efficitur nisi. Fusce
			ullamcorper congue elit dapibus gravida.
		</p>
		<p>
			<strong>Strong tag</strong>
			<br>
			<a href="#" class="">a tag</a>
		</p>
		<p class="uppercase">
			Uppercase
		</p>

	</div>
	<div class=" my-10">
		<h3>Buttons</h3>
		<div class="p-5 bg-yellow-500">
			<div class="btn-set">
				<a href="#" class="y-btn">y-btn</a>
				<a href="#" class="y-btn btn-inverted">btn-inverted</a>
				<a href="#" class="y-btn btn-white">btn-white</a>
				<a href="#" class="y-btn btn-black">btn-black</a>
			</div>

		</div>

	</div>
	<div class="border-t-2 border-b-2 py-10">
		<h3>lists</h3>

		<div class="flex gap-20 md:gap-36">
			<div>
				<h5>ul</h5>
				<ul>
					<li>List item</li>
					<li>List item</li>
					<li>List item</li>
					<li>List item</li>
					<li>List item</li>
				</ul>
			</div>
			<div>
				<h5>ol</h5>
				<ol>
					<li>List item</li>
					<li>List item</li>
					<li>List item</li>
					<li>List item</li>
					<li>List item</li>
				</ol>
			</div>

		</div>

	</div>
	<div class="border-b-2 py-10">
		<h3>Colours</h3>

		<div class="grid md:grid-cols-3 gap-10 md:gap-36">
			<div>
				<h5>bg-grey</h5>
				<div class="bg-grey h-32"></div>
			</div>
			<div>
				<h5>bg-grey-light</h5>
				<div class="bg-grey-light h-32"></div>
			</div>
			<div>
				<h5>bg-grey-dark</h5>
				<div class="bg-grey-dark h-32"></div>
			</div>
			<div>
				<h5>bg-black</h5>
				<div class="bg-black h-32"></div>
			</div>
			<div>
				<h5>bg-white</h5>
				<div class="bg-white h-32 border-2"></div>
			</div>

		</div>


	</div>
	<div class="border-b-2 py-10">
		<h3>Standard form elements</h3>
		<section>
			<form action="">
				<div class="grid grid-cols-4 gap-10">
					<div>
						<label for="fname">Label</label><br>
						<input type="text" placeholder="text input">
					</div>

					<textarea name="" id="" cols="30" rows="10" placeholder="Textarea"></textarea><br>

					<div>
						<input type="submit" value="input submit"> <br>
						<button>Button</button>
					</div>

					<div>
						<select name="" id="">
							<option value="">Select</option>
							<option value="">Option 1</option>
							<option value="">Option 2</option>
							<option value="">Option 3</option>
						</select>
					</div>

					<div>
						<label for="">Check boxes</label> <br>
						<input type="checkbox">
						<input type="checkbox">
						<input type="checkbox">
					</div>
					<div>
						<label for="">Radio</label> <br>
						<input type="radio">
						<input type="radio">
						<input type="radio">
					</div>

					<div>
						<label for="">Range</label> <br>
						<input type="range"> <br>

					</div>
					<div>
						<input type="date">

					</div>
					<div>
						<input type="file">
					</div>

				</div>
			</form>
		</section>


	</div>
	<div class="border-b-2 py-10">
		<h3>Gravity form</h3>

		<p class="text-red-500">Note: Add a gravity form with the id of 1</p>

		<section>
			<?php gravity_form(1, false, false, false, null, true); ?>
		</section>

	</div>
</div>


<!--<section class="blog-listing">-->
<!--	<div class="container">-->
<!--		<div class="row">-->
<!--			--><?php
//			if (have_posts()) :
//				echo '<div class="news-row-wrapper">';
//				while (have_posts()) : the_post();
//					echo '<div class="col-12 col-sm-4 mb-5">';
//					get_template_part('template-parts/news-item');
//					echo '</div>';
//				endwhile;
//				echo '</div>';
//				echo '<div class="row">';
//				echo '<div class="yp-pagination">';
//				// Previous/next page navigation.
//				the_posts_pagination(array(
//						'screen_reader_text' => ' ',
//						'prev_text' => __('Previous', 'twentysixteen'),
//						'next_text' => __('Next', 'twentysixteen'),
//						'before_page_number' => '<span class="meta-nav screen-reader-text">' . __('', 'wp-starter') . ' </span>',
//				));
//				echo '</div>';
//				echo '</div>';
//			endif;
//			?>
<!--		</div>-->
<!--	</div>-->
<!--</section>-->

<?php get_footer(); ?>
