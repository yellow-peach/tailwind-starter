<section class="no-results not-found pt-4 pb-5">

	<h1 class="page-title"><?php _e('Nothing Found', 'wp-starter'); ?></h1>

	<div class="page-content">
		<?php if (is_home() && current_user_can('publish_posts')) : ?>

			<p><?php printf(__('Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'twentysixteen'), esc_url(admin_url('post-new.php'))); ?></p>

		<?php elseif (is_search()) : ?>

			<p><?php _e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'wp-starter'); ?></p>
			<?php get_search_form(); ?>

		<?php else : ?>
			<h5><?php _e('It seems we can&rsquo;t find what you&rsquo;re looking for. ', 'wp-starter'); ?></h5>

			<div class="btn-set">
				<a href="<?php echo home_url(); ?>" class="y-btn" title="Return home">Return home</a>
				<a href="<?php echo site_url('/contact/'); ?>" class="y-btn" title="Get in touch">Get in touch</a>
			</div>

		<?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
