<?php get_header(); ?>

<?php echo get_template_part('template-parts/page-banner'); ?>

<section class="blog-listing">
	<div class="container">


				<?php
				if (have_posts()) :

					echo '<div class="news-row-wrapper">';

					while (have_posts()) : the_post();

						echo '<div>';

						get_template_part('template-parts/news-item');

						echo '</div>';

					endwhile;

					echo '</div>';



					echo '<div class="yp-pagination">';

					// Previous/next page navigation.
					the_posts_pagination(array(
							'screen_reader_text' => ' ',
							'prev_text' => __('Previous', 'wp-starter'),
							'next_text' => __('Next', 'wp-starter'),
							'before_page_number' => '<span class="meta-nav screen-reader-text">' . __('', 'wp-starter') . ' </span>',
					));

					echo '</div>';


				endif;
				?>


	</div>
</section>

<?php get_footer(); ?>
