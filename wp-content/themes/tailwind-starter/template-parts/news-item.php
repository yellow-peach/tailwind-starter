<?php
if (!isset($colour_class)) {
	$colour_class = '';
}
if (!isset($button_class)) {
	$button_class = 'y-btn';
} ?>
<article class="news-item">
	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"
	   class="news-thumb">
		<?php if (get_the_post_thumbnail(get_the_ID())) {
			echo get_the_post_thumbnail(get_the_ID(), 'medium_large', array('class' => 'object-cover'));
		} ?>
	</a>
	<h4><a href="<?php the_permalink(); ?>" class="<?php echo $colour_class; ?>"><?php the_title(); ?></a></h4>
	<?php echo '<p class="' . $colour_class . '">' . get_the_excerpt(get_the_ID()) . '</p>'; ?>
	<a href="<?php the_permalink(); ?>" class="<?php echo $button_class; ?>">Read more</a>

</article>
