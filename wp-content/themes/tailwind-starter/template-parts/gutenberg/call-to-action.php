<?php
$block = get_field('call_to_action');
$styling = get_field('block_styling');
$spacing = get_block_spacing();
$colour_class = get_block_colour_class();
$button_class = get_block_button_colour_class();
$btn_set = isset($block['buttons']) ? get_button_set($block['buttons'], ' mt-5', $button_class) : '';

?>


<section
		class="section-wrapper call-to-action <?php if ($spacing) echo $spacing; ?> <?php if ($styling['background_colour']) echo 'block-bg-' . $styling['background_colour']; ?> bg-black">

	<div class="container">
		<?php if ($block['heading']) echo '<h2 class="' . $colour_class . '">' . $block['heading'] . '</h2>'; ?>
		<?php if ($block['content']) echo '<div class="' . $colour_class . '" data-aos="fade-in">' . $block['content'] . '</div>'; ?>

		<?php echo $btn_set; ?>
	</div>

</section>
