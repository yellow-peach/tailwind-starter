const plugin = require('tailwindcss/plugin');
const _ = require("lodash");
const configuration = require('./theme-configuration.json');

module.exports = {
	mode: 'jit',
	configuration,
	purge: {
		content: [
			'./*.php',
			'./*/*.php',
			'./safelist.txt',
			'**/*.php',
		],
	},
	theme: {
		container: {
			...configuration.container
		},
		spacing: configuration.spacing,
		fontSize: configuration.fontSize,
		extend: {
			colors: configuration.colors,
			fontFamily: configuration.fontFamily,
			width: configuration.width,
			height: configuration.height,
			transitionProperty: configuration.transition,
			zIndex: configuration.zIndex,
			spacing: configuration.spacing
		},
	},
	plugins: [
		require("@tailwindcss/forms")({
			strategy: 'class',
		}),
	]
};
